<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: white">
                <div style="background-color: black; color: white;">
                    <h2>DEVICES</h2>
                </div>
                <table border="1">
                    <tr  style="color:white; background-color: black">
                        <th>Device</th>
                        <th>Made in</th>
                        <th>Price</th>
                        <th>Peripheral</th>
                        <th>Cooler</th>
                        <th>Type</th>
                        <th>Ports</th>
                    </tr>
                    <xsl:for-each select="ComponentCatalogue/Component">
                        <tr>
                            <td>
                                <xsl:value-of select="name"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="price"/>
                            </td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="periferal = 'true'">Yes</xsl:when>
                                    <xsl:otherwise>No</xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="cooler = 'true'">Have</xsl:when>
                                    <xsl:otherwise>Haven't</xsl:otherwise>
                                </xsl:choose>
                            </td>
                            <td>
                                <xsl:value-of select="type"/>
                            </td>
                            <td>
                                <xsl:for-each select="ports/port">
                                    <xsl:value-of select="."/>
                                    <xsl:text>, </xsl:text>
                                </xsl:for-each>
                            </td>

                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
