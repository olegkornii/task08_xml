package SAX;

import Elements.Component;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXRead {
    /**
     * Reads components from xml file
     * @param xml file
     */
    public void read(String xml) {

        final List<Component> components = new ArrayList<>();

        try {
            final SAXParserFactory saxPF = SAXParserFactory.newInstance();
            SAXParser sP = saxPF.newSAXParser();
            DefaultHandler dH = new DefaultHandler() {
                Component component;
                List<String> portList = new ArrayList<>();
                boolean name = false;
                boolean origin = false;
                boolean price = false;
                boolean pereferal = false;
                boolean cooler = false;
                boolean type = false;
                boolean ports = false;
                boolean port = false;


                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) {
                    if (qName.equalsIgnoreCase("NAME")) {
                        component = new Component();
                        name = true;
                    } else if (qName.equalsIgnoreCase("ORIGIN")) {
                        origin = true;
                    } else if (qName.equalsIgnoreCase("PRICE")) {
                        price = true;
                    } else if (qName.equalsIgnoreCase("periferal")) {
                        pereferal = true;
                    } else if (qName.equalsIgnoreCase("COOLER")) {
                        cooler = true;
                    } else if (qName.equalsIgnoreCase("TYPE")) {
                        type = true;
                    } else if (qName.equalsIgnoreCase("PORTS")) {
                        ports = true;
                    } else if (qName.equalsIgnoreCase("PORT")) {
                        port = true;
                    }
                }

                @Override
                public void characters(char[] ch, int start, int length) {

                    if (name) {

                        System.out.println("Name : " + new String(ch, start, length));
                        component.setName(new String(ch, start, length));
                        name = false;
                    }
                    if (origin) {
                        component.setOrigin(new String(ch, start, length));
                        origin = false;
                    }
                    if (price) {
                        component.setPrice(Integer.parseInt(new String(ch, start, length)));
                        price = false;
                    }
                    if (pereferal) {
                        component.setPeripheral(Boolean.parseBoolean(new String(ch, start, length)));
                        pereferal = false;
                    }
                    if (cooler) {
                        component.setCooler(Boolean.parseBoolean(new String(ch, start, length)));
                        pereferal = false;
                    }
                    if (type) {
                        System.out.println(new String(ch, start, length));
                        component.setType(new String(ch, start, length));
                        type = false;
                    }
                    if (ports) {

                        ports = false;
                    }
                    if (port) {
                        String strPort = new String(ch, start, length);
                        portList.add(strPort);
                        System.out.println(new String(ch, start, length));
                        component.setPorts(portList);
                        port = false;
                    }
                }

                @Override
                public void endElement(String uri, String localName, String qName) {
                    if (qName.equalsIgnoreCase("Component")) {
                        components.add(component);
                    }
                }
            };
            sP.parse(xml, dH);
            System.out.println(components);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

    }
}
