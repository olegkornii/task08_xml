package ComponentComparator;

import Elements.Component;

import java.util.Comparator;

/**
 * Represent component comparator
 */
public class ComponentComparator implements Comparator<Component> {
    /**
     * Compere two components by price
     * @param o1 first component
     * @param o2 second component
     * @return differece in price
     */
    @Override
    public int compare(Component o1, Component o2) {
        return o1.getPrice() - o2.getPrice();
    }
}
