package XSLConvert;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class Converter {
    /**
     * Converts xml to html
     * @param xml
     * @param xsl
     */
    public static void convert(Source xml, Source xsl, String HTML) {

        StringWriter sw = new StringWriter();
        try {
            FileWriter fw = new FileWriter(HTML);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer(xsl);
            transformer.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();
            System.out
                    .println("devices.html generated successfully.");
        } catch (IOException | TransformerException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        final String HTMLPATH = "/home/oleg/home/oleg/Документи/IdeaProjects/EpamXMLTask/src/main/resources/componentHTML.html";
        final String XMLPATH = "/home/oleg/home/oleg/Документи/IdeaProjects/EpamXMLTask/src/main/resources/componentXML.xml";
        final String XSLPATH = "/home/oleg/home/oleg/Документи/IdeaProjects/EpamXMLTask/src/main/resources/componentXSL.xsl";

        Source xml = new StreamSource(new File(XMLPATH));
        Source xsl = new StreamSource(XSLPATH);
        convert(xml, xsl,HTMLPATH);
    }
}
