package DOM;

import Elements.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomRead {
    /**
     * Reads components from xlm file
     * @param xml file
     */
    public void read(String xml) {
        List<Component> componentList = new ArrayList<>();
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(xml);
            Node node = document.getDocumentElement();
            System.out.println("List of components :");
            NodeList nodeList = node.getChildNodes();

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node component = nodeList.item(i);
                Component deviceComponent = new Component();

                if (component.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) component;

                    deviceComponent.setName(element.getElementsByTagName("name").item(0).getTextContent());
                    deviceComponent.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                    deviceComponent.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                    deviceComponent.setCooler(Boolean.parseBoolean(element.getElementsByTagName("cooler").item(0).getTextContent()));
                    deviceComponent.setPeripheral(Boolean.parseBoolean(element.getElementsByTagName("periferal").item(0).getTextContent()));
                    deviceComponent.setType(element.getElementsByTagName("type").item(0).getTextContent());
                    deviceComponent.setPorts(getPorts(element.getElementsByTagName("ports")));
                    componentList.add(deviceComponent);

                }
            }
            for (Component c :
                    componentList) {
                System.out.println(c);
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads ports from component
     * @param nodeList list of component nodes
     * @return list of ports
     */
    private List getPorts(NodeList nodeList) {
        List<String> ports = new ArrayList<>();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            NodeList nodes = element.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) node;
                    ports.add(el.getTextContent());
                }
            }
        }

        return ports;
    }
}
