package DOM;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DOMWrite {
    /**
     * Writes component in xmlfile
     * @param xml file
     */
    public void write(String xml) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(xml);
            addNewComponent(document,xml);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds new component in element
     * @param document
     * @param xml file
     */
    private void addNewComponent(Document document, String xml) {
        Node root = document.getDocumentElement();

        Element component = document.createElement("Component");
        Element name = document.createElement("name");
        name.setTextContent("AMD Vega");
        Element origin = document.createElement("origin");
        origin.setTextContent("USA");
        Element price = document.createElement("price");
        price.setTextContent("2500");
        Element periferal = document.createElement("periferal");
        periferal.setTextContent("false");
        Element cooler = document.createElement("cooler");
        cooler.setTextContent("true");
        Element type = document.createElement("type");
        type.setTextContent("GPU");

        component.appendChild(name);
        component.appendChild(origin);
        component.appendChild(price);
        component.appendChild(periferal);
        component.appendChild(cooler);
        component.appendChild(type);
        component.appendChild(ports(document, "TCP, USBTPC, USBTPB"));

        root.appendChild(component);
        writeInDocument(document,xml);
    }

    /**
     * Writes in doceument
     * @param document
     * @param xml
     */
    private void writeInDocument(Document document, String xml) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(document);
            FileOutputStream fos = new FileOutputStream(xml);
            StreamResult streamResult = new StreamResult(fos);
            transformer.transform(source, streamResult);
        } catch (FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Split components from string of ports and adds them in document
     * @param document
     * @param str
     * @return
     */
    private Element ports(Document document, String str) {
        Element ports = document.createElement("ports");
        String pattern = "\\w++";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(str);
        while (m.find()) {
            Element port = document.createElement("port");
            port.setTextContent(m.group());
            ports.appendChild(port);
        }
        return ports;
    }
}
