package STAX;

import Elements.Component;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class STAXReader {
    /**
     * Reads components from xlm
     * @param xml file
     */
    public void read(String xml) {

        List<Component> components = new ArrayList<>();
        List<String> ports = new ArrayList<>();

        try {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            Component component = new Component();
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while (xmlEventReader.hasNext()) {

                XMLEvent event = xmlEventReader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    if (name.equalsIgnoreCase("Component")) {
                        component = new Component();
                    }
                    switch (name) {
                        case "name": {
                            event = xmlEventReader.nextEvent();
                            component.setName(event.asCharacters().getData());
                            break;
                        }
                        case "origin": {
                            event = xmlEventReader.nextEvent();
                            component.setOrigin(event.asCharacters().getData());
                            break;
                        }
                        case "price": {
                            event = xmlEventReader.nextEvent();
                            component.setPrice(Integer.parseInt(event.asCharacters().getData()));
                            break;
                        }
                        case "periferal": {
                            event = xmlEventReader.nextEvent();
                            component.setPeripheral(Boolean.parseBoolean(event.asCharacters().getData()));
                            break;
                        }
                        case "cooler": {
                            event = xmlEventReader.nextEvent();
                            component.setCooler(Boolean.parseBoolean(event.asCharacters().getData()));
                            break;
                        }
                        case "type": {
                            event = xmlEventReader.nextEvent();
                            component.setType(event.asCharacters().getData());
                            break;
                        }
                        case "port": {
                            event = xmlEventReader.nextEvent();
                            ports.add(event.asCharacters().getData());
                            break;
                        }
                    }
                }
                if (event.isEndElement()) {
                    EndElement endElement = event.asEndElement();
                    String end = endElement.getName().getLocalPart();
                    switch (end) {
                        case "ports": {
                            component.setPorts(ports);
                            break;
                        }
                        case "Component": {
                            components.add(component);
                            break;
                        }
                    }
                }
            }
            System.out.println(components);
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
