package Elements;

import DOM.DOMWrite;
import DOM.DomRead;
import SAX.SAXRead;
import STAX.STAXReader;
import XSD.XSDValidator;


public class Test {
    public static void main(String[] args) {
        String xml = "/home/oleg/home/oleg/Документи/IdeaProjects/EpamXMLTask/src/main/resources/componentXML.xml";
        String xsd = "/home/oleg/home/oleg/Документи/IdeaProjects/EpamXMLTask/src/main/resources/componentXSD.xsd";
        XSDValidator xsdValidator = new XSDValidator();
        System.out.println(xsdValidator.validate(xml, xsd));
        DOMWrite domWrite = new DOMWrite();
        domWrite.write(xml);
        DomRead domRead = new DomRead();
        domRead.read(xml);
        SAXRead saxRead = new SAXRead();
        saxRead.read(xml);
        STAXReader staxReader = new STAXReader();
        staxReader.read(xml);

    }
}
